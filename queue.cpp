/**
 * @file queue.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-05-19
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <vector>
#include <algorithm>
#include <iostream>
#include <cmath>
#include "queue.hpp"

/**
 * @brief Construct a new Queue:: Queue object
 * 
 * @details Detailni popis na nekolik radku
 * 
 * @param s Velikost fronty
 */

Queue::Queue(int s):size(s){
    vect.resize(s); // vytvoreni vektoru o velikosti s
}

Queue::~Queue(){
    std::cout << "Byl zavolan destruktor tridy Queue" << std::endl;
}

void Queue::push(int value){
    vect.at(position) = value;
    position++;
}

int Queue::pop(void){
    --position;
    return vect.at(position);
}

std::ostream &operator<<(std::ostream &out, Queue &q){
    for(int i=0; i<q.position; i++){
        out << q.vect.at(i) << " ";
    }
    return out;
}

void Queue::sortASC(void){
    std::vector<int>::iterator it = vect.begin() + position;    // ukazatel na konec fronty
    std::sort(vect.begin(), it, [](int a, int b){return a<b;});
}

void Queue::sortDESC(void){
    std::vector<int>::iterator it = vect.begin() + position;    // ukazatel na konec fronty
    std::sort(vect.begin(), it, [](int a, int b){return a>b;});
}
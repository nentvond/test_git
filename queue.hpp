#include <vector>
#include <iostream>

#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

class Queue{
private:
    std::vector<int> vect;
    int size = 5;
    int position = 0;

public:
    Queue(int s);       // konstruktor tridy Queue s 1 arg.
    ~Queue(void);       // destruktor tridy Queue
    void push(int value);
    int pop(void);
    void sortASC(void);
    void sortDESC(void);

    friend std::ostream &operator<<(std::ostream &out, Queue &q);
};




#endif
#include <iostream>
#include "queue.hpp"

int main(void){
    Queue fronta(10);

    for(int i=0; i<10; i++){
        fronta.push(i+1);
    }
    std::cout << fronta << std::endl;

    fronta.sortDESC();
    std::cout << "serazena sestupne: " << fronta << std::endl;

    fronta.sortASC();
    std::cout << "serazena vzestupne: " << fronta << std::endl;

    return 0;
}